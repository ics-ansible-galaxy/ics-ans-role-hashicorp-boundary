import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_services(host):
    controller = host.file("/etc/boundary.d/controller.hcl")
    assert controller.contains("controller")
    worker = host.file("/etc/boundary.d/worker.hcl")
    assert worker.contains("worker")


def test_running_enabled(host):
    controller = host.service("boundary-controller.service")
    assert controller.is_running
    assert controller.is_enabled
    worker = host.service("boundary-worker.service")
    assert worker.is_running
    assert worker.is_enabled
