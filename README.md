# ics-ans-role-hashicorp-boundary

An Ansible role to deploy [Hashicorp Boundary](https://www.boundaryproject.io/).

## Requirements & Dependencies

- A KMS solution (Hashicorp Vault, AWS KMS, GSPC KMS or Azure Key Vault)
- A PostgreSQL database, with the boundary user and database (UTF8 schema) should be preconfigured (the boundary user must full ownership of the Database).
- Target nodes should be either in `boundary_controller`, `boundary_worker` or both Ansible groups.

## Role Variables

```yaml
boundary_version: '0.7.4'
boundary_user: 'boundary'
boundary_group: 'boundary'
boundary_create_account: true
boundary_db_init_flags: '-skip-initial-login-role-creation'
boundary_kms_type: 'aead' # awskms, gcpckms, transit 

# Set worker tags to manage regions/zones
boundary_set_worker_tags: true
boundary_worker_region: cslab
boundary_worker_region_types:
  - test
  - dev
...
```

### Key Management

One of the Boundary KMS types from [https://www.boundaryproject.io/docs/configuration/kms]

As these choices are radically different depending on your KMS, refer to one of these examples:

- [GCP CKMS](docs/kms_gcp.md)
- [AWS KMS](docs/kms_aws.md)
- [Hashicorp Vault - Transit secrets engine](docs/kms_transit.md)
- [Static AEAD Keys](docs/kms_aead.md) - not suitable for production use

The role default KMS type is set to static AEAD key configuration documented at [https://www.boundaryproject.io/docs/getting-started]

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-hashicorp-boundary
```
- Example [Boundary Playbook](docs/playbook.md)
- Example [Boundary Group Vars](docs/group_vars.md)

### Thanks to the original role authors

Many thanks to [Jacob Mammoliti, Bas Meijer and Jo Rhett](https://github.com/ansible-community/ansible-role-boundary)

## License

BSD 2-clause
